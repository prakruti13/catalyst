## Synopsis

This PHP scripts is executed form the command line that accepts a CSV file as input and insertes valid csv records into a MySQL database.


## Installation

The database name is "myDB" and needs to change as per requirements. sample users.csv file is included in source.

## sample run example

you can see the description of all the options available to run the script by running below command

```
php user_upload.php --help

```
***Dry run example***

```
php user_upload.php --dry_run --file "users.csv"

```