<?php
//setup only valid arguments.

$shortopts = "";
$shortopts .= "u:";
$shortopts .= "p:";
$shortopts .= "h:";

$longopts = array(
		"file:",
		"dry_run",
		"create_table",
		"help"
);
$options  = getopt($shortopts, $longopts);

$conn   = false;
$dbname = "myDB"; //database name assumption

//set database connection if possible
if (isset($options['u']) && isset($options['p']) && isset($options['h'])) {
		$conn = new mysqli($options['h'], $options['u'], $options['p'], $dbname);
		// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}
}

//display options description for help and exit
if (isset($options['help'])) {
		
		$help = '--file [csv_file_name.csv] – provide CSV file to be parsed ' . "\n";
		
		$help .= '--create_table – creates/rebuilt users table (no further action will be taken)' . "\n";
		
		$help .= '--dry_run – this will be used with the --file directive to parse and validate file content. All other functions will be executed,but the database won\'t be altered.' . "\n";
		
		$help .= '-u – MySQL username' . "\n";
		
		$help .= '-p – MySQL password' . "\n";
		
		$help .= '-h – MySQL host' . "\n";
		
		echo $help;
		
		exit;
}

// create table option logic
if (isset($options['create_table'])) {
		if (isset($options['u']) && isset($options['p']) && isset($options['h'])) {
				
				if ($conn) {
						//drop existing table if exist
						$sql = "DROP TABLE IF EXISTS `User`";
						$conn->query($sql);
						//create new user table
						$sql = "CREATE TABLE User (
			name VARCHAR(30) NOT NULL,
			surname VARCHAR(30) NOT NULL,
			email VARCHAR(50) NOT NULL UNIQUE )";
						
						if ($conn->query($sql) === TRUE) {
								echo "Table User created successfully \n";
								exit;
						} else {
								echo "Error creating table: " . $conn->error . "\n";
								exit;
						}
				}
		} else {
				echo "Provide conection credentials \n";
				exit;
		}
		exit;
}

//default variable setup
$dry_run  = false;
$filename = false;

//check if filename is available and file exist
if (isset($options['file']) && !empty($options['file']) && file_exists($options['file'])) {
		
		$ext = explode('.', $options['file']);
		
		if ($ext != 'csv') {
				echo "Only csv file types are allowed \n";
				exit;
		}
		
		$filename = $options['file']; // set file name
}

//dry run logic
if (isset($options['dry_run'])) {
		//if file exist
		if ($filename) {
				$dry_run = true;
		} else {
				echo "Provide file name \n";
				exit;
		}
}



if ($filename) {
		$fp = fopen($filename, "r");
		//skip first header line from file
		fgetcsv($fp);
		$c = 2;
		//read the content from second line
		while (($line = fgetcsv($fp, 0, ",")) !== FALSE) {
				//check for valid email
				if (!validate_email($line[2])) {
						echo "line No " . $c . " contains invalid email. \n";
				} else {
						$name    = ucwords(strtolower($line[0]));
						$surname = ucwords(strtolower($line[1]));
						$email   = strtolower($line[2]);
						if (!$dry_run) {
								
								if (!$conn) {
										echo "Provide conection credentials \n";
										exit;
								}
								$importSQL = 'INSERT INTO user VALUES("' . $name . '","' . $surname . '","' . $email . '")';
								$conn->query($importSQL);
						}
						
				}
				$c++;
		}
		
		
		fclose($fp);
}
if ($conn) {
		$conn->close();
}

function validate_email($e)
{
		return (bool) preg_match("`^[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_\`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$`i", trim($e));
}
?>