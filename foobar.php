<?php 

$sep = '';
$string='';

for( $i=1; $i<=100; $i++ )
{
	//no divisible by both 3 and 5
	if($i % 15 === 0)
	{
		$string .= $sep. 'foobar';
	}
	//no divisible by 3
	elseif($i % 3 === 0)
	{
		$string .= $sep. 'foo';
	}
	//no divisible by 5
	elseif($i % 5 === 0)
	{
		$string .= $sep. 'bar';
	}
	else
	{
		$string .= $sep. $i;

	}
	$sep=', ';

}
echo $string." \n";
?>